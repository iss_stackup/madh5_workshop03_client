# Workshop03Client

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.0.4.

## Development server

Run `ng serve -o --proxy-config proxy.config.js` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build --base-href /workshop03/` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

