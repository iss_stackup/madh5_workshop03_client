import { Injectable } from "@angular/core";
import { HttpClient, HttpParams, HttpHandler, HttpHeaders } from "@angular/common/http";

export interface Customer {
    custId: number;
    name: string;
    addr1: string;
    addr2?: string;
    city: string;
    state: string;
    zip: string;
    phone?: string;
    fax?: string;
    email?: string;
    discount?: string;
    credit?: number;
};

@Injectable()
export class CustomerService { 
    constructor(private http: HttpClient) { }

    getCustomer(custId: number): Promise<Customer> {
        return(
            this.http.get<Customer>(`api/customer/${custId}`)
                .toPromise()
        );
    }

    addCustomer(customer: Customer): Promise<any> {
        const params = new HttpParams()
            .set('custId', customer.custId + '')
            .set('name', customer.name)
            .set('addr1', customer.addr1)
            .set('addr2', customer.addr2)
            .set('city', customer.city)
            .set('state', customer.state)
            .set('zip', customer.zip)
            .set('phone', customer.phone)
            .set('fax', customer.fax)
            .set('email', customer.email)
            .set('discount', customer.discount)
            .set('credit', customer.credit + '');
        const headers = new HttpHeaders()
            .set('Content-Type', 'application/x-www-form-urlencoded');

        return (
            this.http.post<any>('api/customer', params.toString(), { headers })
                .toPromise()
        );
    }

}