import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { CustomerService, Customer } from '../customer.service';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {

  readonly DISCOUNT_CODE = [ 'H', 'M', 'L', 'N' ];
  readonly ZIP = [ '10095', '10096', '12347', '48124', '48128', 
      '85638', '94043', '94401', '95035', '95051', '95117' ]

  notFound = false;

  @ViewChild('form') form: NgForm;

  customer: Customer = {
    custId: null, name: "", addr1: "", addr2: "", city: "", state: "",
    zip: "", phone: "", fax: "", email: "", discount: "", credit: null
  };

  constructor(private customerSvc: CustomerService) { }

  ngOnInit() { }

  search() {
    const custId = this.form.value.custId;
    this.customerSvc.getCustomer(this.form.value.custId)
      .then(result => {
        console.info('Result: ', result);
        this.customer = result;
      })
      .catch(error => {
        this.notFound = error.status == 404;
        console.error('Error: ', error);
      });
  }

  add() { 
    this.customerSvc.addCustomer(this.form.value as Customer)
      .then(() => { this.form.resetForm() })
      .catch(error => {
        console.error('Error: ', error)
      });
  }

}
